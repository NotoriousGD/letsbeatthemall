package com.mygdx.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.game.MyGdxGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		//OpenAL error
		System.setProperty("user.name","\\xD0\\x90\\xD0\\xB4\\xD0\\xBC\\xD0\\xB8\\xD0\\xBD\\xD0\\xB8\\xD1\\x81\\xD1\\x82\\xD1\\x80\\xD0\\xB0\\xD1\\x82\\xD0\\xBE\\xD1\\x80");

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new MyGdxGame(), config);
		config.height = 620;
		config.width = 1600;
	}
}
