package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.mygdx.game.world.Game;

/**
 * Created by Администратор on 13.08.2016.
 */
public class ScreenGame implements Screen {

    com.badlogic.gdx.Game mainGame;
    Game builder;
    FPSLogger log= new FPSLogger();
    public ScreenGame(com.badlogic.gdx.Game mainGame){
        this.mainGame = mainGame;
    };

    @Override
    public void show() {
        try {
            builder= new Game();
            builder.loadSystems();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.57f,0.84f,0.89f,0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        builder.step(delta);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        builder.destroy();
    }

    @Override
    public void dispose() {

    }
}
