package com.mygdx.game.uiObjects.button;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.uiObjects.Touched;

/**
 * Created by Администратор on 30.09.2016.
 */
public class ComponentTouchControlButton extends Touched implements Component{
    public boolean isTouched = false;
    public Vector2 param;

    public ComponentTouchControlButton(Vector2 param, float positionX, float positionY, float width, float height) {
        super(positionX, positionY, width, height);
        this.param = param;

    }

    @Override
    public void action(float tx, float ty) {
        isTouched = true;
    }

    @Override
    public void back(int finger) {
        if(this.finger == finger)
        isTouched = false;
    }
}
