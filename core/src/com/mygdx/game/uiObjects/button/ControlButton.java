package com.mygdx.game.uiObjects.button;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.zLvlMaker.TexturesCompainer;

/**
 * Created by Администратор on 30.09.2016.
 */
public class ControlButton extends Entity{
    public Sprite sprite= new Sprite(new Texture("Textures/stick2.png"));

    public boolean isTouched(){
        if(this.getComponent(ComponentTouchControlButton.class).isTouched)
            return true;
        else
            return false;
    }

    public ControlButton(Vector2 value,float x, float y, float width, float height) {
        this.add(new ComponentTouchControlButton(value,x,y,width,height));
        this.add(new ComponentDrawControlButton(sprite,x,y,width,height));
    }

}
