package com.mygdx.game.uiObjects.button;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.world.Game;

/**
 * Created by Администратор on 30.09.2016.
 */
public class ComponentDrawControlButton implements Component {

    public Sprite sprite;

    public ComponentDrawControlButton(Sprite sprite, float x, float y, float width, float height){
        this.sprite = sprite;
        sprite.setSize(width, height);
        sprite.setPosition(x- Game.XCOL/2,y- Game.YCOL/2);
    };

    public void draw(SpriteBatch batch) {
        sprite.draw(batch);
    }
}
