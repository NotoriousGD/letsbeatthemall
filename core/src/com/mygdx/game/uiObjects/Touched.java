package com.mygdx.game.uiObjects;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Администратор on 16.08.2016.
 */
public abstract class Touched {
    public float x;
    public float y;
    public float height;
    public float width;
    public float radius;
    public boolean isCircle;
    public int finger;

    public Touched(float positionX, float positionY, float width, float height) {
        this.x = positionX;
        this.y = positionY;
        this.height = height;
        this.width = width;
        this.isCircle = false;
    }

    public Touched(float positionX, float positionY, float radius) {
        this.x = positionX;
        this.y = positionY;
        this.radius = radius;
        this.isCircle = true;
    }

    public void touch(float tx, float ty,int finger){
        if(isCircle) {
            Vector2 pos = new Vector2(tx,ty);
            if (pos.dst(x+radius,y+radius) <= radius){
                this.finger = finger;
                action(tx, ty);
            }
        }else
        if(!isCircle) {
            if (tx >= x && tx <= (x + width) && ty >= y && ty <= y + height){
                this.finger = finger;
                action(tx, ty);
            }
        }
    }

    public abstract void action(float tx, float ty);
    public abstract void back(int finger);
}
