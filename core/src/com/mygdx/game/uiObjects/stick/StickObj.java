package com.mygdx.game.uiObjects.stick;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Администратор on 15.08.2016.
 */
public class StickObj extends Entity{
    public Sprite stick1= new Sprite(new Texture("Textures/stick.png"));
    public Sprite stick2= new Sprite(new Texture("Textures/stick2.png"));

    public StickObj(float x, float y, float sizeBack, final float sizeStick){
        this.add(new StickDrawComponent(stick1,stick2,x,y,sizeBack,sizeStick));
        this.add(new StickTouchComponent(x, y, sizeBack/2, stick2));
    }
    public Vector2 getParam(){
        return this.getComponent(StickTouchComponent.class).StickParam;
    }

}


