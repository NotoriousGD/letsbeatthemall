package com.mygdx.game.uiObjects.stick;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.world.Game;

/**
 * Created by Администратор on 16.08.2016.
 */
public class StickDrawComponent implements Component {
    public Sprite back;
    public Sprite stick;
    public StickDrawComponent(Sprite back, Sprite stick, float x, float y, float radiusBack, float radiusStick){
        this.back = back;
        this.stick = stick;
        back.setSize(radiusBack,radiusBack);
        stick.setSize(radiusStick,radiusStick);
        back.setPosition(x- Game.XCOL/2,y- Game.YCOL/2);
        stick.setPosition(
                (x- Game.XCOL/2)+radiusBack/2-radiusStick/2,
                (y- Game.YCOL/2)+radiusBack/2-radiusStick/2);
    };

    public void draw(SpriteBatch batch) {
        back.draw(batch);
        stick.draw(batch);
    }
}
