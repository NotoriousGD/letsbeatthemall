package com.mygdx.game.uiObjects.stick;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.uiObjects.Touched;
import com.mygdx.game.world.Game;

/**
 * Created by Администратор on 16.08.2016.
 */
public class StickTouchComponent extends Touched implements Component {
    public Sprite stick1;
    public Vector2 StickParam = new Vector2();
    Vector2 position= new Vector2();
    boolean touched;

    public StickTouchComponent(float positionX, float positionY, float radius, Sprite stick1) {
        super(positionX, positionY, radius);
        this.stick1 = stick1;
    }

    @Override
    public void action(float tx, float ty) {
        touched = true;
    }

    public void moveStick(float tx, float ty,int finger){
        if(touched && this.finger==finger){
            position.set(tx,ty);position.sub(x+radius,y+radius);
            if(position.dst(0,0) >= radius){
                position.setLength(radius);
                }
            stick1.setPosition(
                    x+radius+position.x-stick1.getHeight()/2- Game.XCOL/2,
                    y+radius+position.y-stick1.getHeight()/2- Game.YCOL/2);
            StickParam.set(position.x/radius,position.y/radius);
        }
    }

    @Override
    public void back(int finger) {
        if(this.finger == finger){
            touched = false;
            stick1.setPosition(
                    (x- Game.XCOL/2)+radius-stick1.getHeight()/2,
                    (y- Game.YCOL/2)+radius-stick1.getHeight()/2);
            StickParam.set(0,0);
        }
    }
}
