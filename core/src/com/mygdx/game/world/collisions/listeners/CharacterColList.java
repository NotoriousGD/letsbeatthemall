package com.mygdx.game.world.collisions.listeners;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.mygdx.game.ashley.systems.SystemDestroyer;
import com.mygdx.game.gameObjects.character.ComponentCharacterState;
import com.mygdx.game.ashley.conponents.ComponentGetable;
import com.mygdx.game.world.collisions.CollisionListener;
import com.mygdx.game.world.collisions.CollisionSystem;

/**
 * Created by Администратор on 19.08.2016.
 */
public class CharacterColList implements CollisionListener {

    @Override
    public void onBeginContact(Contact contact) {
            if(contact.getFixtureA().getFilterData().categoryBits == CollisionSystem.CHARACTER_BOTTOM)isGround(contact.getFixtureA(),contact.getFixtureB());
            if(contact.getFixtureB().getFilterData().categoryBits == CollisionSystem.CHARACTER_BOTTOM)isGround(contact.getFixtureB(),contact.getFixtureA());

            if(contact.getFixtureA().getFilterData().categoryBits == CollisionSystem.CHARACTER_TOP)isTopGroundStart(contact.getFixtureA(),contact.getFixtureB());
            if(contact.getFixtureB().getFilterData().categoryBits == CollisionSystem.CHARACTER_TOP)isTopGroundStart(contact.getFixtureB(),contact.getFixtureA());


            if(contact.getFixtureA().getFilterData().groupIndex == CollisionSystem.GROUP_GET)get(contact.getFixtureB(),contact.getFixtureA());
            if(contact.getFixtureB().getFilterData().groupIndex == CollisionSystem.GROUP_GET)get(contact.getFixtureA(),contact.getFixtureB());


        }

    @Override
    public void onEndedContact(Contact contact) {
        if(contact.getFixtureA().getFilterData().categoryBits == CollisionSystem.CHARACTER_TOP)isTopGroundEnd(contact.getFixtureA(),contact.getFixtureB());
        if(contact.getFixtureB().getFilterData().categoryBits == CollisionSystem.CHARACTER_TOP)isTopGroundEnd(contact.getFixtureB(),contact.getFixtureA());
    }


    @Override
    public void onPreContact(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void onPostContact(Contact contact, ContactImpulse impulse) {

    }

    private void isGround(Fixture character, Fixture object){
        if(object.getFilterData().groupIndex == CollisionSystem.GROUP_GROUND){
            ((ComponentCharacterState) character.getUserData()).OnGround = true;
            System.out.print("GROUND");
            character.getBody().getFixtureList().get(0).setFriction(0.2f);
            character.getBody().getFixtureList().get(1).setFriction(0.2f);
            ((ComponentCharacterState) character.getUserData()).velocity =
                    ((ComponentCharacterState) character.getUserData()).BUFvelocity;
            System.out.print(((ComponentCharacterState) character.getUserData()).OnGround);
        }
    }

    private void get(Fixture character, Fixture object){
        ((ComponentCharacterState)character.getUserData()).get(
                ((ComponentGetable)object.getUserData()).type,
                ((ComponentGetable)object.getUserData()).value
        );
        SystemDestroyer.addDel(
                ((ComponentGetable)object.getUserData()).obj,
                object.getBody());
    }
    private void isTopGroundStart(Fixture character, Fixture object){
        if(object.getFilterData().groupIndex == CollisionSystem.GROUP_GROUND){
            ((ComponentCharacterState) character.getUserData()).velocity = 0f;
        }
    }
    private void isTopGroundEnd(Fixture character, Fixture object){
        if(object.getFilterData().groupIndex == CollisionSystem.GROUP_GROUND){
            ((ComponentCharacterState) character.getUserData()).velocity =
                    ((ComponentCharacterState) character.getUserData()).BUFvelocity;
        }
    }

}
