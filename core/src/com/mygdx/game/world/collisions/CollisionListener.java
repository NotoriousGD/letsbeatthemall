package com.mygdx.game.world.collisions;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

/**
 * Created by Администратор on 19.08.2016.
 */
public interface CollisionListener {
    void onBeginContact(Contact contact);
    void onEndedContact(Contact contact);
    void onPreContact(Contact contact, Manifold oldManifold);
    void onPostContact(Contact contact, ContactImpulse impulse);
}
