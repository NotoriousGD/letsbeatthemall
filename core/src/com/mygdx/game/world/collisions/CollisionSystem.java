package com.mygdx.game.world.collisions;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.world.collisions.listeners.CharacterColList;


import java.util.HashMap;
import java.util.List;

/**
 * Created by Администратор on 19.08.2016.
 */
public class CollisionSystem implements ContactListener {


    public static short CHARACTER_BOTTOM    = 0x0002;
    public static short CHARACTER_TOP       = 0x0004;

    public static short GROUP_CHARACTER  =   0x0002;
    public static short GROUP_MOB        =   0x0004;
    public static short GROUP_GROUND     =   0x0008;
    public static short GROUP_HIT        =   0x0010;
    public static short GROUP_GET        =   0x0020;

    private final HashMap<Short,CollisionListener> collisionListeners;

    public CollisionSystem(World world) {
        this.collisionListeners = new HashMap<Short,CollisionListener>();
        world.setContactListener(this);
        collisionListeners.put(GROUP_CHARACTER,new CharacterColList());
    }


    @Override
    public void beginContact(Contact contact) {
        if(contact.getFixtureA().getFilterData().groupIndex == GROUP_CHARACTER ||
                contact.getFixtureB().getFilterData().groupIndex == GROUP_CHARACTER      )
            collisionListeners.get(GROUP_CHARACTER).onBeginContact(contact);
    }

    @Override
    public void endContact(Contact contact) {
        if(contact.getFixtureA().getFilterData().groupIndex == GROUP_CHARACTER ||
                contact.getFixtureB().getFilterData().groupIndex == GROUP_CHARACTER      )
            collisionListeners.get(GROUP_CHARACTER).onEndedContact(contact);
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
        if(contact.getFixtureA().getFilterData().groupIndex == GROUP_CHARACTER ||
                contact.getFixtureB().getFilterData().groupIndex == GROUP_CHARACTER      )
            collisionListeners.get(GROUP_CHARACTER).onPostContact(contact,impulse);
    }

}
