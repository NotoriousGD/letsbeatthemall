package com.mygdx.game.world;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.ashley.systems.SystemCharacter;
import com.mygdx.game.ashley.systems.SystemInput;
import com.mygdx.game.ashley.systems.SystemRedraw;
import com.mygdx.game.ashley.systems.SystemDraw;
import com.mygdx.game.ashley.systems.SystemDestroyer;
import com.mygdx.game.uiObjects.stick.StickObj;
import com.mygdx.game.world.collisions.CollisionSystem;
import com.mygdx.game.zLvlMaker.Builder;
import com.mygdx.game.zLvlMaker.Loader;


/**
 * Created by Администратор on 14.08.2016.
 */
public class Game {
    static float ASPECTRATIO = (float)Gdx.graphics.getWidth()/(float)Gdx.graphics.getHeight();
    public static float XCOL=(32*ASPECTRATIO);
    public static float YCOL=32;


    public static float WORLD_STEP = 1/60f;
    public static int WORLD_VELOCITY = 4;
    public static int WORLD_POSITION = 4;
    public static World world;

    public Engine engine;
    float stepTime;

    CollisionSystem collision;

    OrthographicCamera camera;
    Box2DDebugRenderer renderBox;
    SpriteBatch batch;
    StickObj stick = new StickObj(1,1,15,4);
    Loader loader ;
    Builder builder;


    public Game() throws Exception {
        world = new World(new Vector2(0,-20f),true);
        collision = new CollisionSystem(world);
        engine = new Engine();
        batch = new SpriteBatch();
        this.camera = new OrthographicCamera(Game.XCOL, Game.YCOL);
        camera.position.add(Game.XCOL/2, Game.YCOL/2,0);

        loader = Loader.getInstance();
        loader.loadLvl(1,"Objects/Character_debug.json");

        builder = new Builder(engine);
        builder.addCharacter(10,10);
        builder.addUI();
        builder.run();
        renderBox = new Box2DDebugRenderer();
    }

    public void loadSystems(){
        //engine.addEntity(stick);
        engine.addSystem(new SystemDestroyer(10,engine));
        engine.addSystem(new SystemCharacter(1,batch,camera));
        engine.addSystem(new SystemRedraw(1));
        engine.addSystem(new SystemInput(5,camera));
        engine.addSystem(new SystemDraw(0,batch,camera));
    }

    public void step(float delta){
        batch.setProjectionMatrix(camera.combined);
        camera.update();

        engine.update(delta);
        renderBox.render(world,camera.combined);
        stepTime += delta;
        if (stepTime>=WORLD_STEP){
            while(stepTime>=WORLD_STEP){
                world.step(WORLD_STEP,WORLD_VELOCITY,WORLD_POSITION);
                stepTime-=delta;
            }
        }
    }
    public void destroy() {
        world.dispose();
        engine.removeAllEntities();
    }

}
