package com.mygdx.game.gameObjects.character;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.utils.JsonValue;
import com.mygdx.game.ashley.conponents.ComponentDrawAnimation;


import java.util.HashMap;

/**
 * Created by Администратор on 21.08.2016.*/

public class Character extends Entity {

    private float height,width;
    public Character(
            JsonValue object,
            float x, float y,
            HashMap<ComponentCharacterState.State, ComponentDrawAnimation> animations
    )
    {
        height = object.get("Body").getFloat("height");
        width  = object.get("Body").getFloat("width");
        this.add(
                new ComponentCharacterDraw(
                        animations
                )
        );
        this.add(
                new ComponentCharacterState(
                        object.get("State")
                )
        );
        this.add(
                new ComponentCharacterBody(
                        object.get("Body"),x,y
                )
        );

        for(Fixture buf:this.getComponent(ComponentCharacterBody.class).body.getFixtureList()){
                buf.setUserData(this.getComponent(ComponentCharacterState.class));
        }
    }

    public float getHeight() {
        return height;
    }

    public float getWidth() {
        return width;
    }
}
