package com.mygdx.game.gameObjects.gameObject;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.utils.JsonValue;
import com.mygdx.game.ashley.conponents.ComponentDrawAnimation;
import com.mygdx.game.ashley.conponents.ComponentDrawSimple;
import com.mygdx.game.ashley.conponents.ComponentLayer;

/**
 * Created by Администратор on 22.08.2016.
 */
public class ObjectNobody extends Entity {

    public ObjectNobody(JsonValue object,
                        float positionX,
                        float positionY,
                        int layer
    )
    {
        this.add(
                ComponentLayer.createLayComponent(layer)
        );
        if(object.get("texture").getString("renderType").equals("animation"))
            this.add(
                    new ComponentDrawAnimation(
                            object.get("texture"),
                            object.getString("atlas"),
                            positionX,
                            positionY,
                            layer
                    )
            );

        if(object.get("texture").getString("renderType").equals("simple"))
            this.add(
                    new ComponentDrawSimple(
                            object.get("texture"),
                            object.getString("atlas"),
                            positionX,
                            positionY,
                            layer
                    )
            );
    }
}
