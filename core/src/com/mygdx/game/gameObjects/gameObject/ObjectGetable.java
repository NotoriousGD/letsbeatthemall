package com.mygdx.game.gameObjects.gameObject;

import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.utils.JsonValue;
import com.mygdx.game.ashley.conponents.ComponentBody;
import com.mygdx.game.ashley.conponents.ComponentGetable;
import com.mygdx.game.world.collisions.CollisionSystem;

/**
 * Created by Администратор on 21.08.2016.
 */
public class ObjectGetable extends ObjectSimple {

    public ObjectGetable(
                        JsonValue object,
                         float positionX,
                         float positionY,
                         int layer
    )
    {
        super(
                object,
                positionX,
                positionY,
                layer
        );
        addGet(
                object.get("state").getString("getType"),
                object.get("state").getInt("getValue")
        );
    }

    public void addGet(String getType, int value){
        this.add(
                new ComponentGetable(
                        getType,
                        value,
                        this
                )
        );
        Filter filter = new Filter();
        filter.groupIndex = CollisionSystem.GROUP_GET;
       // this.getComponent(ComponentBody.class).body.getFixtureList().first().setUserData(this.getComponent(ComponentBody.class));
       // this.getComponent(ComponentBody.class).body.getFixtureList().first().setFilterData(filter);

    }
}
