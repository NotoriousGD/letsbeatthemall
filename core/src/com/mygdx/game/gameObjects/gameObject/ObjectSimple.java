package com.mygdx.game.gameObjects.gameObject;


import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.utils.JsonValue;
import com.mygdx.game.ashley.conponents.ComponentBody;
import com.mygdx.game.ashley.conponents.ComponentDrawAnimation;
import com.mygdx.game.ashley.conponents.ComponentDrawSimple;
import com.mygdx.game.ashley.conponents.ComponentLayer;
import com.mygdx.game.ashley.conponents.ComponentRedraw;

/**
 * Created by Администратор on 13.08.2016.
 */
public class ObjectSimple extends Entity{

    public ObjectSimple(
            JsonValue object,
            float positionX,
            float positionY,
            int layer
    )
    {
        System.out.print("check");
        this.add(
                ComponentLayer.createLayComponent(layer)
        );
        this.add(
                new ComponentBody(
                    object.get("body"),
                    positionX,
                    positionY
                )
        );

        if(object.get("texture").getString("renderType").equals("animation"))
            this.add(
                    new ComponentDrawAnimation(
                            object.get("texture"),
                            object.getString("atlas"),
                            positionX,
                            positionY,
                            layer
                    )
            );

        if(object.get("texture").getString("renderType").equals("simple"))
            this.add(
                    new ComponentDrawSimple(
                            object.get("texture"),
                            object.getString("atlas"),
                            positionX,
                            positionY,
                            layer
                    )
            );

        if(object.get("body").getString("type").equals("dynamic"))
            this.add(
                    new ComponentRedraw()
            );
    }
}
