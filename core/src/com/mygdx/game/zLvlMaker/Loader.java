package com.mygdx.game.zLvlMaker;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.mygdx.game.ashley.conponents.ComponentDrawAnimation;
import com.mygdx.game.gameObjects.character.Character;
import com.mygdx.game.gameObjects.character.ComponentCharacterState;

import java.util.HashMap;

/**
 * Created by Администратор on 14.08.2016.
 */
public class Loader implements Runnable {
//Lvl
    private HashMap<String, JsonValue> RESOURCES;
    private Array<JsonValue> INITIAL;

//Character
    private JsonValue CHARACTER;
    private HashMap<ComponentCharacterState.State, ComponentDrawAnimation> CharacterAnimations
            = new HashMap<ComponentCharacterState.State, ComponentDrawAnimation>();

//For initial class
    private int ID;
    private String characterPath;

    private Loader(){
        RESOURCES = new HashMap<String, JsonValue>();
        INITIAL = new Array<JsonValue>();
    }
    private static Loader instance;
    public static Loader getInstance(){
        if(instance == null)instance = new Loader();
        return instance;
    }

    public JsonValue getAllLvls(){
        return new JsonReader().parse(Gdx.files.getFileHandle("Lvls.json", Files.FileType.Internal));
    }

    public HashMap<ComponentCharacterState.State, ComponentDrawAnimation> getCharacterAnimations(){
        return CharacterAnimations;
    }
    public JsonValue getCharacter(){return CHARACTER;}
    public void loadLvl(int LVLid,String characterPath){
        ID=LVLid;
        this.characterPath = characterPath;
        run();
    }

    public Array<JsonValue> getInitial(){
        return INITIAL;
    }
    public JsonValue getResources(String name){return RESOURCES.get(name);}

    @Override
    public void run() {
        RESOURCES.clear();
        System.out.println(getAllLvls().get("lvls").get(ID).getString("path"));
        JsonValue init = new JsonReader().parse(Gdx.files.getFileHandle(getAllLvls().get("lvls").get(ID).getString("path"), Files.FileType.Internal));
        for(int i = 0 ; i<init.get("Initial").size;i++){
            INITIAL.add(init.get("Initial").get(i));
        }
        for(int i = 0 ; i<init.get("Types").size;i++){
            JsonValue buf = new JsonReader().parse(Gdx.files.getFileHandle(init.get("Types").getString(i), Files.FileType.Internal));
            for(int j = 0 ; j<buf.get("Objects").size;j++){
                RESOURCES.put(
                        buf.get("Objects").get(j).getString("name"),
                        buf.get("Objects").get(j));
            }
        }

        CharacterAnimations.clear();
        CHARACTER = new JsonReader().parse(Gdx.files.getFileHandle(characterPath, Files.FileType.Internal));
        JsonValue animations = CHARACTER.get("texture").get("statesAnimations");
        for(int i =0;i<animations.size;i++){

            CharacterAnimations.put(
                    stringToState(animations.get(i).getString("state")),
                    new ComponentDrawAnimation(animations.get(i), CHARACTER.get("texture").getString("atlas"),0,0,3)
            );

        }
    }
    private ComponentCharacterState.State stringToState(String state){
        if(state.equals("stand")) return ComponentCharacterState.State.STAND;
        if(state.equals("jump")) return ComponentCharacterState.State.JUMP;
        if(state.equals("death")) return ComponentCharacterState.State.DEATH;
        if(state.equals("walk")) return ComponentCharacterState.State.WALK;
        if(state.equals("kick1")) return ComponentCharacterState.State.KICK1;
        if(state.equals("kick2")) return ComponentCharacterState.State.KICK2;
        if(state.equals("use1")) return ComponentCharacterState.State.USE1;
        if(state.equals("use2")) return ComponentCharacterState.State.USE2;
        if(state.equals("use3")) return ComponentCharacterState.State.USE3;
        else return ComponentCharacterState.State.STAND;
    }
}
