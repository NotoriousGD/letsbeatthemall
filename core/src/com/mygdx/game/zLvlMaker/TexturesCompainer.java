package com.mygdx.game.zLvlMaker;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;

import java.util.HashMap;

/**
 * Created by Администратор on 15.08.2016.
 */
public class TexturesCompainer {
    private HashMap<String, TextureAtlas> types = new HashMap<String, TextureAtlas>();


    private static TexturesCompainer instance;
    public static TexturesCompainer getInstance(){
        if(instance == null)instance = new TexturesCompainer();
        return instance;
    }

    public TexturesCompainer() {
        types.put("Los",new TextureAtlas("Textures/Los.atlas"));
        types.put("Warrior",new TextureAtlas("Textures/WarriorChaer.pack"));
    }

    public Sprite getSprite(String atlas,String spriteName){
        return types.get(atlas).createSprite(spriteName);
    }
    public Array<Sprite> getAnimationFrames(String atlas, Array<String> frames){
        Array<Sprite> animation = new Array<Sprite>();
        for(int i = 0; i<frames.size; i++){
            animation.add(types.get(atlas).createSprite(frames.get(i)));
        }
        return animation;
    }

}
