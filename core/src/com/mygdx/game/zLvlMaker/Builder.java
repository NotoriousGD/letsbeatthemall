package com.mygdx.game.zLvlMaker;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonValue;
import com.mygdx.game.gameObjects.character.Character;
import com.mygdx.game.gameObjects.gameObject.ObjectGetable;
import com.mygdx.game.gameObjects.gameObject.ObjectNobody;
import com.mygdx.game.gameObjects.gameObject.ObjectSimple;
import com.mygdx.game.uiObjects.button.ControlButton;

/**
 * Created by Администратор on 11.09.2016.
 */
public class Builder implements Runnable {

    public Character mainCharacter;

    Engine ashley;
    Array<JsonValue> INITIAL;
    Loader loader = Loader.getInstance();

    public Builder(Engine engine) {
        ashley = engine;
        INITIAL =  loader.getInitial();//bad
    }

    @Override
    public void run() {
        System.out.println("RUN");

        for(JsonValue init:INITIAL){
            JsonValue res = loader.getResources(init.getString("name"));
            if(res.getString("objectType").equals("simple")) addSimpleObject(res,init);
            if(res.getString("objectType").equals("get")) addGetObject(res,init);
            if(res.getString("objectType").equals("nobody")) addNobodyObject(res,init);
        }
    }

    private void addSimpleObject(JsonValue object,JsonValue initial){
        System.out.println("ObjectAdded");
        for(int i = 0; i < initial.get("Coord").size; i++){
            ashley.addEntity(new ObjectSimple(
                    object,
                    initial.get("Coord").get(i).getFloat("x"),
                    initial.get("Coord").get(i).getFloat("y"),
                    initial.getInt("layer")));
        }
        for(int i = 0; i < initial.get("Generate").size; i++){
            float x = initial.get("Generate").get(i).getFloat("startX");
            float y = initial.get("Generate").get(i).getFloat("startY");
            for(int j = 0; j < initial.get("Generate").get(i).getFloat("count")+1; j++){
                ashley.addEntity(new ObjectSimple(
                        object,
                        x,
                        y,
                        initial.getInt("layer")
                ));
                x+=initial.get("Generate").get(i).getFloat("stepX");
                y+=initial.get("Generate").get(i).getFloat("stepY");
            }
        }
    };

    private void addGetObject(JsonValue object,JsonValue initial){
        for(int i = 0; i < initial.get("Coord").size; i++){
            ashley.addEntity(new ObjectGetable(
                    object,
                    initial.get("Coord").get(i).getFloat("x"),
                    initial.get("Coord").get(i).getFloat("y"),
                    initial.getInt("layer")
            ));
        }

        for(int i = 0; i < initial.get("Generate").size; i++){
            float x = initial.get("Generate").get(i).getFloat("startX");
            float y = initial.get("Generate").get(i).getFloat("startY");
            for(int j = 0; j < initial.get("Generate").get(i).getFloat("count")+1; j++){
                ashley.addEntity(new ObjectGetable(
                        object,
                        x,
                        y,
                        initial.getInt("layer")
                ));
                x+=initial.get("Generate").get(i).getFloat("stepX");
                y+=initial.get("Generate").get(i).getFloat("stepY");
            }
        }

    };

    private void addNobodyObject(JsonValue object,JsonValue initial){
        for(int i = 0; i < initial.get("Coord").size; i++){
            ashley.addEntity(new ObjectNobody(
                    object,
                    initial.get("Coord").get(i).getFloat("x"),
                    initial.get("Coord").get(i).getFloat("y"),
                    initial.getInt("layer")
            ));
        }

        for(int i = 0; i < initial.get("Generate").size; i++){
            float x = initial.get("Generate").get(i).getFloat("startX");
            float y = initial.get("Generate").get(i).getFloat("startY");
            for(int j = 0; j < initial.get("Generate").get(i).getFloat("count")+1; j++){
                ashley.addEntity(new ObjectNobody(
                        object,
                        x,
                        y,
                        initial.getInt("layer")
                ));
                x+=initial.get("Generate").get(i).getFloat("stepX");
                y+=initial.get("Generate").get(i).getFloat("stepY");
            }
        }

    };

    public void addCharacter(float x, float y){
        ashley.addEntity( new Character(loader.getCharacter(),x,y,loader.getCharacterAnimations()));
    }

    public void addUI(){
        ashley.addEntity(new ControlButton(new Vector2(1,0),8,5,4,4));
        ashley.addEntity(new ControlButton(new Vector2(-1,0),2,5,4,4));
        ashley.addEntity(new ControlButton(new Vector2(0,5),5,7,4,4));
    }
}
