package com.mygdx.game.ashley.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.gameObjects.character.ComponentCharacterBody;
import com.mygdx.game.gameObjects.character.ComponentCharacterDraw;
import com.mygdx.game.gameObjects.character.ComponentCharacterState;
import com.mygdx.game.uiObjects.button.ComponentDrawControlButton;
import com.mygdx.game.uiObjects.button.ComponentTouchControlButton;
import com.mygdx.game.uiObjects.stick.StickObj;

/**
 * Created by Администратор on 18.08.2016.
 */
public class SystemCharacter extends EntitySystem {
    BitmapFont font =  new BitmapFont();

    SpriteBatch batch;
    OrthographicCamera camera;
    private ComponentMapper<ComponentCharacterBody>  charBody  = ComponentMapper.getFor(ComponentCharacterBody.class);
    private ComponentMapper<ComponentCharacterDraw>  charDraw  = ComponentMapper.getFor(ComponentCharacterDraw.class);
    private ComponentMapper<ComponentCharacterState> charState = ComponentMapper.getFor(ComponentCharacterState.class);

    private ComponentMapper<ComponentTouchControlButton> controllerComponents = ComponentMapper.getFor(ComponentTouchControlButton.class);

    private ImmutableArray<Entity> character;

    private ImmutableArray<Entity> controller;


    ComponentCharacterBody body;
    ComponentCharacterDraw draw;
    ComponentCharacterState state;

    public SystemCharacter(int priority, SpriteBatch batch, OrthographicCamera camera) {
        super(priority);
        this.batch  = batch;
        this.camera = camera;
        font.getData().setScale(0.1f);
    }

    public void addedToEngine(Engine engine){
        character = engine.getEntitiesFor(
                Family.all(ComponentCharacterDraw.class,
                        ComponentCharacterBody.class,
                        ComponentCharacterState.class).get());
        controller = engine.getEntitiesFor(
                Family.all(ComponentTouchControlButton.class).get()
        );

        body = charBody.get(character.get(0));
        draw = charDraw.get(character.get(0));
        state = charState.get(character.get(0));
    }
    public void update(float deltaTime) {
        camera.position.set(body.getBody().getPosition().x,body.getBody().getPosition().y,0);
        batch.begin();
        draw.draw(batch,deltaTime,body.getBody().getPosition().sub(body.center));
        font.draw(batch, "F1="+body.getBody().getFixtureList().get(1).getFriction(), body.getBody().getPosition().x, body.getBody().getPosition().y+5);
        font.draw(batch, "F2="+body.getBody().getFixtureList().get(0).getFriction(), body.getBody().getPosition().x, body.getBody().getPosition().y+6);
        font.draw(batch, "G="+state.OnGround, body.getBody().getPosition().x, body.getBody().getPosition().y+7);
        font.draw(batch, "x="+body.getBody().getLinearVelocity().x, body.getBody().getPosition().x, body.getBody().getPosition().y+8);
        font.draw(batch, "y="+body.getBody().getLinearVelocity().y, body.getBody().getPosition().x, body.getBody().getPosition().y+9);
        batch.end();

        if(controllerComponents.get(controller.get(0)).isTouched){
            walkR();
            setState(ComponentCharacterState.State.WALK);
        }
        if(controllerComponents.get(controller.get(1)).isTouched){
            walkL();
            setState(ComponentCharacterState.State.USE1);
        }
        if(!controllerComponents.get(controller.get(0)).isTouched && !controllerComponents.get(controller.get(1)).isTouched){
            stop();
            setState(ComponentCharacterState.State.STAND);
        }
        if(controllerComponents.get(controller.get(2)).isTouched){
            jump();
            setState(ComponentCharacterState.State.JUMP);
        }
    }

    public void setState(ComponentCharacterState.State state) {
        this.state.charState = state;
        draw.setState(state);
    }

    public void walkR() {
        if(Math.abs(body.getBody().getLinearVelocity().x) < state.maxSpeed){
            body.getBody().getFixtureList().get(0).setFriction(0f);
            body.getBody().getFixtureList().get(1).setFriction(0f);
            body.getBody().applyLinearImpulse(
                    state.velocity*1*body.getBody().getMass(),0,
                    body.getBody().getPosition().x,body.getBody().getPosition().y,true
            );
        }
    }

    public void walkL() {
        if(Math.abs(body.getBody().getLinearVelocity().x) < state.maxSpeed){
            body.getBody().getFixtureList().get(0).setFriction(0f);
            body.getBody().getFixtureList().get(1).setFriction(0f);
            body.getBody().applyLinearImpulse(
                    -state.velocity*1*body.getBody().getMass(),0,
                    body.getBody().getPosition().x,body.getBody().getPosition().y,true
            );
        }
    }

    public void jump() {
        if(state.OnGround){
            body.getBody().setLinearVelocity(body.getBody().getLinearVelocity().x,0);
            body.getBody().setTransform(body.getBody().getPosition().x,body.getBody().getPosition().y+0.01f,0);
            body.getBody().applyLinearImpulse(0,20*body.getBody().getMass(),body.getBody().getPosition().x,body.getBody().getPosition().y,true);
            body.getBody().getFixtureList().get(0).setFriction(100f);
            body.getBody().getFixtureList().get(1).setFriction(100f);
            state.OnGround= false;
        }
    }

    public void stop() {
        body.getBody().getFixtureList().get(0).setFriction(100f);
        body.getBody().getFixtureList().get(1).setFriction(100f);
    }

}

