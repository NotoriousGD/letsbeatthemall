package com.mygdx.game.ashley.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.ashley.conponents.ComponentDrawAnimation;
import com.mygdx.game.ashley.conponents.ComponentDrawSimple;
import com.mygdx.game.ashley.conponents.ComponentLayer;
import com.mygdx.game.uiObjects.button.ComponentDrawControlButton;

/**
 * Created by Администратор on 13.08.2016.
 */
public class SystemDraw extends EntitySystem{
    SpriteBatch batch,UIbatch;
    OrthographicCamera camera;
    private Array<ImmutableArray<Entity>> entitiesA = new Array<ImmutableArray<Entity>>();
    private Array<ImmutableArray<Entity>> entitiesS = new Array<ImmutableArray<Entity>>();
    private ImmutableArray<Entity> entitiesCB;

    Engine engine;

    private ComponentMapper<ComponentDrawAnimation> drawAnimation = ComponentMapper.getFor(ComponentDrawAnimation.class);
    private ComponentMapper<ComponentDrawSimple> drawSimple = ComponentMapper.getFor(ComponentDrawSimple.class);

    private ComponentMapper<ComponentDrawControlButton> drawControlButton = ComponentMapper.getFor(ComponentDrawControlButton.class);

    public SystemDraw(int priority, SpriteBatch batch, OrthographicCamera camera) {
        super(priority);
        this.batch = batch;
        UIbatch = new SpriteBatch();
        this.camera = camera;
        entitiesA.setSize(5);
        entitiesS.setSize(5);
    }

    public void addedToEngine(Engine engine) {
        this.engine=engine;
        entitiesA.set(0, engine.getEntitiesFor(Family.all(ComponentLayer.Layer1.class,ComponentDrawAnimation.class).get()));
        entitiesA.set(1, engine.getEntitiesFor(Family.all(ComponentLayer.Layer2.class,ComponentDrawAnimation.class).get()));
        entitiesA.set(2, engine.getEntitiesFor(Family.all(ComponentLayer.Layer3.class,ComponentDrawAnimation.class).get()));
        entitiesA.set(3, engine.getEntitiesFor(Family.all(ComponentLayer.Layer4.class,ComponentDrawAnimation.class).get()));
        entitiesA.set(4, engine.getEntitiesFor(Family.all(ComponentLayer.Layer5.class,ComponentDrawAnimation.class).get()));

        entitiesS.set(0, engine.getEntitiesFor(Family.all(ComponentLayer.Layer1.class,ComponentDrawSimple.class).get()));
        entitiesS.set(1, engine.getEntitiesFor(Family.all(ComponentLayer.Layer2.class,ComponentDrawSimple.class).get()));
        entitiesS.set(2, engine.getEntitiesFor(Family.all(ComponentLayer.Layer3.class,ComponentDrawSimple.class).get()));
        entitiesS.set(3, engine.getEntitiesFor(Family.all(ComponentLayer.Layer4.class,ComponentDrawSimple.class).get()));
        entitiesS.set(4, engine.getEntitiesFor(Family.all(ComponentLayer.Layer5.class,ComponentDrawSimple.class).get()));

        entitiesCB = engine.getEntitiesFor(Family.all(ComponentDrawControlButton.class).get());
    }

    public void update(float deltaTime) {
        batch.begin();
        for (int i = 0; i < 5; ++i) {
            for(int j = 0; j < entitiesA.get(i).size(); j++){
                drawAnimation.get(entitiesA.get(i).get(j)).draw(batch,deltaTime);
            }
            for(int j = 0; j < entitiesS.get(i).size(); j++){
                drawSimple.get(entitiesS.get(i).get(j)).draw(batch,deltaTime);
            }
        }
        batch.end();

        UIbatch.setProjectionMatrix(camera.projection);
        UIbatch.begin();
        for(int i = 0; i < entitiesCB.size(); i++){
            drawControlButton.get(entitiesCB.get(i)).draw(UIbatch);
        }
        UIbatch.end();

    }
}
