package com.mygdx.game.ashley.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.mygdx.game.ashley.conponents.ComponentBody;
import com.mygdx.game.ashley.conponents.ComponentDrawAnimation;
import com.mygdx.game.ashley.conponents.ComponentDrawSimple;
import com.mygdx.game.ashley.conponents.ComponentRedraw;

/**
 * Created by Администратор on 14.08.2016.
 * Drawing position/rotation  = body params
 */
public class SystemRedraw extends EntitySystem {
    private ImmutableArray<Entity> entitiesA,entitiesS;

    Engine engine;

    private  ComponentMapper<ComponentBody> bodies = ComponentMapper.getFor(ComponentBody.class);
    private  ComponentMapper<ComponentDrawAnimation> drawA = ComponentMapper.getFor(ComponentDrawAnimation.class);
    private  ComponentMapper<ComponentDrawSimple> drawS = ComponentMapper.getFor(ComponentDrawSimple.class);

    public SystemRedraw(int priority) {
        super(priority);
    }

    public void addedToEngine(Engine engine) {
        this.engine=engine;
        entitiesA= engine.getEntitiesFor(
                Family.all(
                        ComponentRedraw.class,
                        ComponentDrawAnimation.class
                ).get());

        entitiesS= engine.getEntitiesFor(
                Family.all(
                        ComponentRedraw.class,
                        ComponentDrawSimple.class
                ).get());
      }

    public void update(float deltaTime) {

        for (int i = 0; i < entitiesA.size(); ++i) {
            drawA.get(entitiesA.get(i)).setPosition(bodies.get(entitiesA.get(i)).getCenterPosition());
            drawA.get(entitiesA.get(i)).setRotation(bodies.get(entitiesA.get(i)).getRotation());
        }
        for (int i = 0; i < entitiesS.size(); ++i) {
            drawS.get(entitiesS.get(i)).setPosition(bodies.get(entitiesS.get(i)).getCenterPosition());
            drawS.get(entitiesS.get(i)).setRotation(bodies.get(entitiesS.get(i)).getRotation());
        }
    }
}
