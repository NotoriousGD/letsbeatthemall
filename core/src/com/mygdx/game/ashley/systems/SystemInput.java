package com.mygdx.game.ashley.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.mygdx.game.uiObjects.button.ComponentTouchControlButton;
import com.mygdx.game.world.Game;

/**
 * Created by Администратор on 16.08.2016.
 */
public class SystemInput extends EntitySystem {

    private  float transX(float X)
    {
        return X/(Gdx.graphics.getWidth() / Game.XCOL);
    }
    private  float transY(float Y){
        Y-=Gdx.graphics.getHeight();
        Y*=-1;
        return Y/(Gdx.graphics.getHeight() / Game.YCOL);
    };

    OrthographicCamera camera;

    private ComponentMapper<ComponentTouchControlButton> touchControlButton = ComponentMapper.getFor(ComponentTouchControlButton.class);

    public SystemInput(int priority, OrthographicCamera camera) {
        super(priority); this.camera = camera;
    }

    private ImmutableArray<Entity> controlButton;

    @Override
    public void addedToEngine(Engine engine) {
        controlButton = engine.getEntitiesFor(Family.all(ComponentTouchControlButton.class).get());
    }

    @Override
    public void update(float deltaTime) {
        for (int j = 0; j < 1; ++j) {//fingers
            for (int i = 0; i < controlButton.size(); i++) {//buttons
                if (Gdx.input.isTouched(j)) {
                    touchControlButton.get(controlButton.get(i)).touch(
                            transX(Gdx.input.getX(j)),
                            transY(Gdx.input.getY(j)),
                            j
                    );

                }else
                    touchControlButton.get(controlButton.get(i)).back(j);
            }
        }
    }
}
