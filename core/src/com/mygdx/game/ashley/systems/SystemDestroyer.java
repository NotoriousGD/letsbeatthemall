package com.mygdx.game.ashley.systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.world.Game;

import java.util.Iterator;

/**
 * Created by Администратор on 21.08.2016.
 */
public class SystemDestroyer extends EntitySystem {
    static Array<Entity> DEATHE = new Array<Entity>();
    static Array<Body> DEATHB = new Array<Body>();

    public static void addDel(Entity get,Body body){
        DEATHE.add(get);
        DEATHB.add(body);
    }

    Engine engine;
    public SystemDestroyer(int priority, Engine engine) {
        super(priority); this.engine = engine;
    }

    public void update(float deltaTime) {

        Iterator<Body> j = DEATHB.iterator();
        if(!Game.world.isLocked()){
            while(j.hasNext()){
                Game.world.destroyBody(j.next());
            }
            DEATHB.clear();
        }
        Iterator<Entity> i = DEATHE.iterator();
        while(i.hasNext()){
            engine.removeEntity(i.next());
        }
        DEATHE.clear();
    }
}