package com.mygdx.game.ashley.conponents;

import com.badlogic.ashley.core.Component;
import com.mygdx.game.gameObjects.gameObject.ObjectGetable;

/**
 * Created by Администратор on 21.08.2016.
 */
public class ComponentGetable implements Component {
    public int value;
    public Type type;
    public ObjectGetable obj;
    public ComponentGetable(String getType,int getValue, ObjectGetable link) {
        switch(getType){
            case "hp" :{type = Type.HP; break;}
            case "money" :{type = Type.MONEY; break;}
            case "energy" :{type = Type.ENERGY; break;}
            case "none" :{type = Type.NONE; break;}
        }
        this.value = getValue;
        this.obj = link;
    }
    public enum Type{
        HP,
        MONEY,
        ENERGY,
        NONE
    }
}