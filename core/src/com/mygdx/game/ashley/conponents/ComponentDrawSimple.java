package com.mygdx.game.ashley.conponents;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.JsonValue;
import com.mygdx.game.zLvlMaker.TexturesCompainer;

/**
 * Created by Администратор on 10.09.2016.
 */
public class ComponentDrawSimple  implements Component {
    public Sprite sprite;
    private float marginX;
    private float marginY;
    public int layer = 0;

    public ComponentDrawSimple(
            JsonValue object,
            String atlas,
            float positionX,
            float positionY,
            int layer
    ){
        this.layer = layer;
        sprite = TexturesCompainer.getInstance().getSprite(
                atlas,
                object.getString("texture")
        );
        sprite.setPosition(positionX+marginX,positionY+marginY);
        sprite.setSize(object.getFloat("width"),object.getFloat("height"));
        sprite.setOrigin(object.getFloat("width")/2,object.getFloat("height")/2);
        this.marginX = object.getFloat("marginX");
        this.marginY = object.getFloat("marginY");
    };

    public void draw(SpriteBatch batch,float delta) {
        sprite.draw(batch);
    }
    public void setPosition(Vector2 position){
        sprite.setPosition(position.x+marginX,position.y+marginY);
    }
    public void setRotation(float degrees){
        sprite.setRotation(degrees);
    }
    public Vector2 getPosition(){
        return new Vector2 (sprite.getX(),sprite.getY());
    }

}
