package com.mygdx.game.ashley.conponents;

import com.badlogic.ashley.core.Component;


/**
 * Created by Администратор on 14.09.2016.
 */
public class ComponentLayer {
    public static Component createLayComponent(int num){
        if(num<=1){return new Layer1();}
        if(num==2){return new Layer2();}
        if(num==3){return new Layer3();}
        if(num==4){return new Layer4();}
        else {return new Layer5();}
    }
    public static class Layer1 implements Component {}
    public static class Layer2 implements Component {}
    public static class Layer3 implements Component {}
    public static class Layer4 implements Component {}
    public static class Layer5 implements Component {}
}
