package com.mygdx.game.ashley.conponents;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.JsonValue;
import com.mygdx.game.world.Game;
import com.mygdx.game.world.collisions.CollisionSystem;


/**
 * Created by Администратор on 09.09.2016.
 */
public class ComponentBody implements Component{
    public Body body;
    float height,width ;

    public ComponentBody(
            JsonValue object,
            float positionX,
            float positionY
    )
    {
        this.height = object.getFloat("height");
        this.width =  object.getFloat("width");
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(positionX, positionY);
        if(object.getString("type").toLowerCase().equals("static")){
            bodyDef.type = BodyDef.BodyType.StaticBody;
        }
        if(object.getString("type").toLowerCase().equals("dynamic")){
            bodyDef.type = BodyDef.BodyType.DynamicBody;
        }


        body = Game.world.createBody(bodyDef);
        if(object.getString("shapeType").toLowerCase().equals("rectangle")){
            PolygonShape shape = new PolygonShape();
            shape.setAsBox(width / 2,height / 2 );

            body.createFixture(shape, object.getFloat("density"));
            body.getFixtureList().first().setFriction(object.getFloat("friction"));
            body.getFixtureList().first().setRestitution(object.getFloat("restitution"));

            body.setTransform(
                    positionX + width / 2,
                    positionY + height / 2, 0
            );
            shape.dispose();
        }

        if(object.getString("shapeType").toLowerCase().equals("circle")){System.out.print("CIRC");
            CircleShape shape = new CircleShape();
            shape.setRadius(height / 2);

            body.createFixture(shape, object.getFloat("density"));
            body.getFixtureList().first().setFriction(object.getFloat("friction"));
            body.getFixtureList().first().setRestitution(object.getFloat("restitution"));

            body.setTransform(
                    positionX + width / 2,
                    positionY + height / 2, 0
            );
        }
        addFilter(object.getString("collisionFilter"));
    }

    public Vector2 getCenterPosition() {
        return new Vector2(body.getPosition().x-width/2,body.getPosition().y-height/2);
    }

    public float getRotation() {
        return MathUtils.radiansToDegrees*body.getAngle();
    }

    private void addFilter(String filter){
        Filter collisionFilter= new Filter();;
        if(filter.equals("ground")){collisionFilter.groupIndex = CollisionSystem.GROUP_GROUND;}
        body.getFixtureList().get(0).setFilterData(collisionFilter);
    }
}