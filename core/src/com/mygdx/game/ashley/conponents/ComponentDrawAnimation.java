package com.mygdx.game.ashley.conponents;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.JsonValue;
import com.mygdx.game.zLvlMaker.TexturesCompainer;

/**
 * Created by Администратор on 10.09.2016.
 */
public class ComponentDrawAnimation implements Component
    {
        public static short TYPE_CYCLE = 1;
        public static short TYPE_ONCE  = 2;
        public int layer = 0;
        private int iterator    = 0;

        private float timer;
        private float type;
        private float marginX;
        private float marginY;

        Array<Sprite> sprites = new Array<Sprite>();
        FloatArray timings = new FloatArray();

        public ComponentDrawAnimation(
                JsonValue object,
                String atlas,
                float positionX,
                float positionY,
                int layer
        ) {
            this.layer = layer;
            for(int i = 0;i < object.get("Animation").size;i++){
                this.sprites.add(TexturesCompainer.getInstance().getSprite(
                        atlas,
                        object.get("Animation").get(i).getString("frame")
                    )
                );
                this.timings.add(object.get("Animation").get(i).getFloat("time"));
            }

            this.type = object.getFloat("animationType");

        this.marginX = object.getFloat("marginX");
        this.marginY = object.getFloat("marginY");

        for(Sprite buf:this.sprites){
            buf.setOrigin(object.getFloat("width")/2,object.getFloat("height")/2);
            buf.setSize(object.getFloat("width"),object.getFloat("height"));
            buf.setPosition(positionX+marginX,positionY+marginY);
        }
    }

    public void draw(SpriteBatch batch, float delta){
        if(iterator != -1){
            sprites.get(iterator).draw(batch);
            timer+=delta;
            if(timer>timings.get(iterator)){
                iterator++; timer = 0;
                if(iterator>=sprites.size){
                    if(type==(TYPE_CYCLE)) {iterator =  0;}
                    if(type==(TYPE_ONCE)) {iterator = sprites.size-1;}
                }
            }
        }else{
            sprites.get(sprites.size-1).draw(batch);
        }
    }

    public void setPosition(Vector2 position){
            sprites.get(iterator).setPosition(
                    position.x+marginX,
                    position.y+marginY
            );
    }
        public void reset(){
            iterator   = 0;
        }

    public void setRotation(float degrees){
            sprites.get(iterator).setRotation(degrees);
    }

}
